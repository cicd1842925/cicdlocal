const express = require("express");
const app = express();

const port = 8080;

app.get("/", (req, res) => {
  res.send("This is node app from cicd demo");
});

app.listen(port, () => {
  console.log("app is listening on 8080");
});
